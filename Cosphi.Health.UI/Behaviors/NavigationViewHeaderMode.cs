﻿namespace Cosphi.Health.UI.Behaviors;

public enum NavigationViewHeaderMode
{
    Always,
    Never,
    Minimal
}
