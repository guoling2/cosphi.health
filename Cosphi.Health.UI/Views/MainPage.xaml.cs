﻿using Cosphi.Health.UI.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace Cosphi.Health.UI.Views;

public sealed partial class MainPage : Page
{
    public MainViewModel ViewModel
    {
        get;
    }

    public MainPage()
    {
        ViewModel = App.GetService<MainViewModel>();
        InitializeComponent();
    }
}
