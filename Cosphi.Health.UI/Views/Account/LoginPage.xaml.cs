﻿using Cosphi.Health.UI.ViewModels.Account;
using Microsoft.UI.Xaml.Controls;

// To learn more about WinUI, the WinUI project structure,
// and more about our project templates, see: http://aka.ms/winui-project-info.

namespace Cosphi.Health.UI.Views.Account;
/// <summary>
/// An empty page that can be used on its own or navigated to within a Frame.
/// </summary>
public sealed partial class LoginPage : Page
{
    public LoginPage(LoginViewModel viewModel)
    {
        ViewModel = viewModel;

        this.InitializeComponent();
    }

    public LoginViewModel ViewModel
    {
        get; set;
    }

    private void LoginButton_Click(object sender, Microsoft.UI.Xaml.RoutedEventArgs e)
    {
        if (string.IsNullOrWhiteSpace(ViewModel.UserName))
        {
            App.MainWindow.ShowMessageDialogAsync("用户名不能为空");

            return;
        }

        //登录成功 这里要刷新下页面的内容才行
        if (ViewModel.Verify())
        {
            App.MainWindow.Content = App.GetService<ShellPage>();
        }
          
    }
}
