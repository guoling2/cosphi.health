﻿using CommunityToolkit.WinUI.UI.Controls;

using Cosphi.Health.UI.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace Cosphi.Health.UI.Views;

public sealed partial class ListDetailsPage : Page
{
    public ListDetailsViewModel ViewModel
    {
        get;
    }

    public ListDetailsPage()
    {
        ViewModel = App.GetService<ListDetailsViewModel>();
        InitializeComponent();
    }

    private void OnViewStateChanged(object sender, ListDetailsViewState e)
    {
        if (e == ListDetailsViewState.Both)
        {
            ViewModel.EnsureItemSelected();
        }
    }
}
