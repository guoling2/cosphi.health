﻿using Cosphi.Health.UI.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace Cosphi.Health.UI.Views;

public sealed partial class ContentGridPage : Page
{
    public ContentGridViewModel ViewModel
    {
        get;
    }

    public ContentGridPage()
    {
        ViewModel = App.GetService<ContentGridViewModel>();
        InitializeComponent();
    }
}
