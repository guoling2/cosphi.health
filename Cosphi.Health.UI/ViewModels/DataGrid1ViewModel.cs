﻿using System.Collections.ObjectModel;

using CommunityToolkit.Mvvm.ComponentModel;

using Cosphi.Health.UI.Contracts.ViewModels;
using Cosphi.Health.UI.Core.Contracts.Services;
using Cosphi.Health.UI.Core.Models;

namespace Cosphi.Health.UI.ViewModels;

public class DataGrid1ViewModel : ObservableRecipient, INavigationAware
{
    private readonly ISampleDataService _sampleDataService;

    public ObservableCollection<SampleOrder> Source { get; } = new ObservableCollection<SampleOrder>();

    public DataGrid1ViewModel(ISampleDataService sampleDataService)
    {
        _sampleDataService = sampleDataService;
    }

    public async void OnNavigatedTo(object parameter)
    {
        Source.Clear();

        // TODO: Replace with real data.
        var data = await _sampleDataService.GetGridDataAsync();

        foreach (var item in data)
        {
            Source.Add(item);
        }
    }

    public void OnNavigatedFrom()
    {
    }
}
