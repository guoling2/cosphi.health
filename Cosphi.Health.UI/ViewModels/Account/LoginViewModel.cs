﻿using CommunityToolkit.Mvvm.ComponentModel;
using Cosphi.Health.UI.Contracts.Services;

namespace Cosphi.Health.UI.ViewModels.Account;
public class LoginViewModel : ObservableRecipient
{
    private INavigationService _navigationService;
    public LoginViewModel(INavigationService navigationService)
    {
        _navigationService=navigationService;
    }

    public string UserName
    {
        get;set;
    }

    public string  PassWord
    {
        get;
        set;
    }

    public bool Verify()
    {
        
        return true;
      //  _navigationService.InitializeFrame(rootFrame);
    }
}
