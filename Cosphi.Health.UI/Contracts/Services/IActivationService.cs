﻿namespace Cosphi.Health.UI.Contracts.Services;

public interface IActivationService
{
    Task ActivateAsync(object activationArgs);
}
