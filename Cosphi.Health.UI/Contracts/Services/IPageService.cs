﻿namespace Cosphi.Health.UI.Contracts.Services;

public interface IPageService
{
    Type GetPageType(string key);
}
